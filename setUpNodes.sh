#!/bin/bash
# Set up BATMAN nodes

NODES="root@node20-1 root@node20-10 root@node14-10 root@node20-20"

# load files to nodes
scp -o "StrictHostKeyChecking no" "batman-adv/bat_iv_ogm.c" root@node20-20:/root/batman-adv-2016.2/net/batman-adv/bat_iv_ogm.c
scp -o "StrictHostKeyChecking no" "batman-adv/bat_iv_ogm.c" root@node20-1:/root/batman-adv-2016.2/net/batman-adv/bat_iv_ogm.c

# configure nodes
for node in $NODES; do
    ssh -o "StrictHostKeyChecking no" ${node} "cd batman-adv-2016.2 && make && make install"
    ssh -o "StrictHostKeyChecking no" ${node} "cd batctl-2016.2 && make && make install"
    ssh -o "StrictHostKeyChecking no" ${node} "bash configure.sh"
done
