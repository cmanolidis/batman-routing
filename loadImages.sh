#!/bin/bash
# Load image files to nodes

GROUP=node20-1.grid.orbit-lab.org,node20-20.grid.orbit-lab.org,node20-10.grid.orbit-lab.org,node14-10.grid.orbit-lab.org

omf load -i cmanolidis-node-node20-1.grid.orbit-lab.org-2016-09-21-08-10-15.ndz -t "$GROUP"
omf tell -a on -t "$GROUP"
